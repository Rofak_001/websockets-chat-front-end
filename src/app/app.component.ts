import {Component, ElementRef, OnInit, QueryList, Renderer2, ViewChild, ViewChildren} from '@angular/core';
import {WebsockChatService} from './websock-chat.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'websockets-chat-front-end';
  username;
  sender;
  message;
  sendTo;
  select;
  allUser;
  storeMessage = new Map();
  // tslint:disable-next-line:variable-name
  @ViewChild('div') div: ElementRef;
  @ViewChild('msg_history') msgHistory: ElementRef;

  constructor(private webSocketsChat: WebsockChatService, private renderer2: Renderer2) {
  }

  ngOnInit(): void {
    this.webSocketsChat.data.subscribe(data => {
      // @ts-ignore
      console.log(data.content);
      // @ts-ignore
      if (this.select === data.sender) {
        // @ts-ignore
        this.render(data.content);
      } else {
        // @ts-ignore
        this.storeMessage.set(data.sender, data.content);
        this.storeMessage.set('isNew', true);
        // @ts-ignore
        this.storeMessage.set('sender', data.sender);
        // @ts-ignore
        $('#' + data.sender).css({'background-color': 'yellow'});

      }
    });
  }

  disconnect(): void {
    this.webSocketsChat._disconnect();
  }

  sendMessage(): void {
    this.webSocketsChat._send(this.sender, this.message, this.sendTo);
    const divOutgoing = this.renderer2.createElement('div');
    divOutgoing.classList.add('outgoing_msg');
    const divSentMsg = this.renderer2.createElement('div');
    divSentMsg.classList.add('sent_msg');
    const p = this.renderer2.createElement('p');
    const text = this.renderer2.createText(this.message);
    p.append(text);
    divSentMsg.append(p);
    divOutgoing.append(divSentMsg);
    this.renderer2.appendChild(this.msgHistory.nativeElement, divOutgoing);
  }

  registerUser(): void {
    this.webSocketsChat.registerUser(this.username).subscribe(res => {
      console.log(res);
    });
  }

  getAllUser(): void {
    this.webSocketsChat.getAllUser().subscribe(data => {
      this.allUser = data;
      console.log(data);
    });
  }

  render(message): void {
    // tslint:disable-next-line:variable-name
    const incoming_msg = this.renderer2.createElement('div');
    incoming_msg.classList.add('incoming_msg');
    // tslint:disable-next-line:variable-name
    const received_msg = this.renderer2.createElement('div');
    received_msg.classList.add('received_msg"');
    // tslint:disable-next-line:variable-name
    const received_withd_msg = this.renderer2.createElement('div');
    received_withd_msg.classList.add('received_withd_msg');
    const p = this.renderer2.createElement('p');
    const text = this.renderer2.createText(message);
    p.append(text);
    received_withd_msg.append(p);
    received_msg.append(received_withd_msg);
    incoming_msg.append(received_msg);
    this.renderer2.appendChild(this.msgHistory.nativeElement, incoming_msg);
  }

  selectUser(username): void {
    $('.outgoing_msg').remove();
    $('.incoming_msg').remove();
    this.sender = this.username;
    this.sendTo = username;
    this.select = username;
    if (this.storeMessage.get('sender') === username) {
      if (this.storeMessage.get('isNew')) {
        this.render(this.storeMessage.get(username));
        this.storeMessage.set('isNew', false);
      }
    }

  }
}
