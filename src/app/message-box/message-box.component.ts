import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['../app.component.css']
})
export class MessageBoxComponent implements OnInit {
  @Input() chatWith;

  constructor() {
  }

  ngOnInit(): void {
  }

}
