import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsockChatService {
  webSocketEndPoint = 'http://localhost:8080/chat';
  topic = '/topic/messages/';
  stompClient: any;
  private dataSource = new Subject();
  data = this.dataSource.asObservable();

  constructor(private httpClient: HttpClient) {
  }

  _connect(username): void {
    const chatPublic = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(chatPublic);
    // tslint:disable-next-line:variable-name
    this.stompClient.connect({}, (frames) => {
      this.stompClient.subscribe(this.topic + username, data => {
        this.onMessageReceived(data);
      });
    });
  }

  onMessageReceived(message): void {
    console.log('Message Recieved from Server :: ' + message.body);
    this.dataSource.next(JSON.parse(message.body));
  }

  _disconnect(): void {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

  _send(mySender, myContent, to): void {
    const chatMessage = {
      sender: mySender,
      content: myContent,
      type: 'CHAT',
      time: null
    };
    console.log('calling logout api via web socket');
    this.stompClient.send('/app/chat/' + to, {}, JSON.stringify(chatMessage));
  }

  registerUser(username): Observable<any> {
    this._connect(username);
    return this.httpClient.post('http://localhost:8080/register', {
      name: username
    });
  }

  getAllUser(): Observable<any> {
    return this.httpClient.get('http://localhost:8080/getAll');
  }
}
